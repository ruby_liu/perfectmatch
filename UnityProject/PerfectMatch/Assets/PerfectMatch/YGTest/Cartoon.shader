﻿Shader "Unlit/Cartoon"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" { }
        
        _DiffuseColor ("DiffuseColor", color) = (1, 1, 1, 1)
        _DiffuseRangeMin ("DiffuseRangeMin", Range(0, 1)) = 0.5
        _DiffuseRangeMax ("DiffuseRangeMax", Range(0, 1)) = 0.5
        
        _SpecularColor ("SpecularColor", color) = (1, 1, 1, 1)
        _Glossness ("Glossness", Range(1, 128)) = 70
        
        _EmissionColor ("EmissionColor", color) = (0, 0, 0, 0)
        
        _LightMap ("LightMap", 2D) = "black" { }
        
        _EdgeThickness ("EdgeThickness", float) = 0.53
        _EdgePosZ ("EdgePosZ", Range(0, 1)) = 0
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }
        
        // Pass
        // {
            //     Cull Front
            //     //ZTest LEqual
            //     //ZWrite Off
            
            //     CGPROGRAM
            
            //     #pragma target 3.0
            //     #pragma vertex vert
            //     #pragma fragment frag
            //     #include "UnityCG.cginc"
            
            //     struct v2f
            //     {
                //         float4 pos: SV_POSITION;
                //     };
                //     float _EdgeThickness, _EdgePosZ;
                
                //     v2f vert(appdata_base v)
                //     {
                    //         v2f o;
                    //         v.vertex.y += _EdgePosZ;
                    //         o.pos = UnityObjectToClipPos(v.vertex + v.normal * _EdgeThickness);
                    //         return o;
                    //     }
                    
                    //     half4 frag(v2f i): COLOR
                    //     {
                        //         return half4(0, 0, 0, 0);
                        //     }
                        //     ENDCG
                        
                        // }
                        
                        Pass
                        {
                            Tags { "LightMode" = "ForwardBase" }
                            CGPROGRAM
                            
                            #pragma vertex vert
                            #pragma fragment frag
                            
                            #include "UnityCG.cginc"
                            
                            struct appdata
                            {
                                float4 vertex: POSITION;
                                float2 uv: TEXCOORD0;
                                float3 normal: NORMAL0;
                            };
                            
                            struct v2f
                            {
                                float2 uv: TEXCOORD0;
                                float4 vertex: SV_POSITION;
                                float4 localPos: TEXCOORD1;
                                float3 worldNormal: NORMAL0;
                            };
                            
                            sampler2D _MainTex, _LightMap;
                            float4 _MainTex_ST;
                            float _Glossness, _DiffuseRangeMin, _DiffuseRangeMax;
                            fixed4 _DiffuseColor, _SpecularColor, _EmissionColor;
                            
                            v2f vert(appdata v)
                            {
                                v2f o;
                                o.vertex = UnityObjectToClipPos(v.vertex);
                                o.uv = v.uv;
                                o.localPos = v.vertex;
                                o.worldNormal = normalize(UnityObjectToWorldNormal(v.normal));
                                return o;
                            }
                            
                            fixed4 frag(v2f i): SV_Target
                            {
                                float3 worldView = normalize(WorldSpaceViewDir(i.localPos));
                                float3 worldLight = normalize(WorldSpaceLightDir(i.localPos));
                                
                                float LDotN = dot(worldLight, i.worldNormal) * 0.5 + 0.5;
                                float BdotN = pow(max(0, dot(normalize(worldView + worldLight), i.worldNormal)), _Glossness);
                                fixed4 col = tex2D(_MainTex, i.uv);
                                
                                fixed4 lightMapColor = tex2D(_LightMap, i.uv);
                                
                                fixed3 lambertCol = lerp(_DiffuseColor, 1, smoothstep(_DiffuseRangeMin, _DiffuseRangeMax, LDotN * (1 - lightMapColor.g))) * col.rgb;
                                fixed3 specularCol = _SpecularColor.rgb * step(0.5, BdotN);
                                
                                return fixed4(lambertCol + specularCol + _EmissionColor.rgb, 1);
                                //return lightMapColor;
                            }
                            ENDCG
                            
                        }
                    }
                    Fallback "Diffuse"
                }
