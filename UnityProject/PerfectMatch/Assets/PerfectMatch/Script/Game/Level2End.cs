﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MidnightCoder.Game;

public class Level2End : LevelEndBase
{
    public Transform door;
    public Transform pos1;
    public Transform pos2;
    protected override void Behaviour(PlayerCtl playerCtl)
    {
        if (GameManager.S.data.GetCurLvSelect() == 0)
        {
            End1(playerCtl);
        }
        else
        {
            End2(playerCtl);
        }
    }


    public Animator gounannv;
    public GameObject kissFx;

    private void End1(PlayerCtl playerCtl)
    {
        PlayerFollowCtl followCtl = playerCtl.GetComponent<PlayerFollowCtl>();
        foreach (NpcCtl npc in followCtl.npcs)
        {
            npc.startFollowPlayer = false;
            npc.SetFollowSpeed(3);
        }
        for (int i = 0; i < followCtl.npcs.Count; i++)
        {
            followCtl.npcs[i].followTrans = pos1.GetChild(i);
            followCtl.npcs[i].startFollowPlayer = true;
        }

        Timer.S.DoWait(1f).OnComplete(() =>
        {
            for (int j = 0; j < followCtl.npcs.Count; j++)
            {
                followCtl.npcs[j].anim.SetTrigger("Idle");
            }
        });
        Sequence seq = DOTween.Sequence();
        seq.Append(playerCtl.transform.DOMove(pos1.position, 1).SetEase(Ease.Linear));
        seq.AppendCallback(() =>
        {
            playerCtl.fsm.SetNextState("Idle", true);
        });
        seq.AppendInterval(0.2f);
        seq.AppendCallback(() => { playerCtl.anim.SetTrigger("OpenDoor"); });
        seq.AppendInterval(0.3f);
        seq.Append(door.DOLocalRotate(Vector3.up * 200, 1.5f));
        seq.AppendCallback(() =>
        {
            playerCtl.fsm.SetNextState("Walk", true);
            CameraBase.S.DoEnd();
        });
        seq.Append(playerCtl.transform.DOMove(pos2.position, 2).SetEase(Ease.Linear));
        seq.InsertCallback(3.3f, () =>
        {
            foreach (NpcCtl npc in followCtl.npcs)
            {
                npc.startFollowPlayer = false;
                npc.SetFollowSpeed(6);
            }
            NpcStart(followCtl);
        });
        seq.AppendCallback(() =>
        {
            playerCtl.fsm.SetNextState("Idle", true);
            Timer.S.DoWait(1).OnComplete(() =>
            {
                gounannv.SetTrigger("End");
                kissFx.SetActive(false);
            });
        });
        seq.AppendInterval(5);
        seq.AppendCallback(() =>
        {
            UIManager.S.ClosePanel(UIPanelType.MainPanel);
            GameManager.S.data.AddLevelNum();
            GameManager.S.GoNextLevel();
        });
    }



    private void End2(PlayerCtl playerCtl)
    {
        PlayerFollowCtl followCtl = playerCtl.GetComponent<PlayerFollowCtl>();
        foreach (NpcCtl npc in followCtl.npcs)
        {
            npc.startFollowPlayer = false;
            npc.SetFollowSpeed(3);
        }
        for (int i = 0; i < followCtl.npcs.Count; i++)
        {
            followCtl.npcs[i].followTrans = pos1.GetChild(i);
            followCtl.npcs[i].startFollowPlayer = i != 0;
        }
        Timer.S.DoWait(1f).OnComplete(() =>
        {
            for (int j = 1; j < followCtl.npcs.Count; j++)
            {
                followCtl.npcs[j].anim.SetTrigger("Idle");
            }
        });
        playerCtl.fsm.SetNextState("Idle", true);
        Sequence seq = DOTween.Sequence();
        seq.Append(followCtl.npcs[0].transform.DOMove(pos1.position, 1));
        seq.AppendCallback(() => { followCtl.npcs[0].anim.SetTrigger("End"); });
        seq.AppendInterval(0.15f);
        seq.Append(door.DOLocalRotate(Vector3.up * 200, 1.5f));
        seq.InsertCallback(2.3f, () =>
        {
            followCtl.npcs[0].anim.SetTrigger("Move");
            followCtl.npcs[0].startFollowPlayer = true;
        });
        seq.AppendInterval(1);
        seq.AppendCallback(() =>
        {
            followCtl.npcs[0].anim.SetTrigger("Idle");
            CameraBase.S.DoEnd();
            playerCtl.fsm.SetNextState("Walk", true);
        });
        seq.Append(playerCtl.transform.DOMove(pos2.position, 2).SetEase(Ease.Linear));
        seq.InsertCallback(4.4f, () =>
        {
            foreach (NpcCtl npc in followCtl.npcs)
            {
                npc.startFollowPlayer = false;
                npc.SetFollowSpeed(6);
            }
            NpcStart(followCtl);
        });
        seq.AppendCallback(() =>
        {
            playerCtl.fsm.SetNextState("Idle", true);
            playerCtl.anim.SetTrigger("ClownKick");
            Timer.S.DoWait(1).OnComplete(() =>
            {
                gounannv.SetTrigger("End");
                kissFx.SetActive(false);
            });

        });
        seq.AppendInterval(6);
        seq.AppendCallback(() =>
        {
            UIManager.S.ClosePanel(UIPanelType.MainPanel);
            GameManager.S.data.AddLevelNum();
            GameManager.S.GoNextLevel();
        });


    }


    private void NpcStart(PlayerFollowCtl followCtl)
    {
        for (int i = 0; i < followCtl.npcs.Count; i++)
        {
            //followCtl.npcs[i].followTrans = pos1.GetChild(i);
            followCtl.npcs[i].startFollowPlayer = true;
            followCtl.npcs[i].anim.SetTrigger("Move");
            followCtl.npcs[i].anim.speed = 2f;
        }

        Timer.S.DoWait(3.2f).OnComplete(() =>
        {
            for (int i = 0; i < followCtl.npcs.Count; i++)
            {
                followCtl.npcs[i].anim.speed = 1;
            }
        });
        Timer.S.DoWait(3.5f).OnComplete(() =>
        {
            for (int i = 0; i < followCtl.npcs.Count; i++)
            {
                followCtl.npcs[i].DoLast();
            }
        });
        pos1.GetComponent<Animator>().enabled = true;
    }

    //[SerializeField] private Transform pos1;
    //[SerializeField] private Transform pos2;
    //private Animator[] npcAnims;
    //protected override void Behaviour(PlayerCtl playerCtl)
    //{
    //    npcAnims = pos2.GetComponentsInChildren<Animator>();
    //    foreach (var each in npcAnims)
    //    {
    //        each.SetTrigger("Ready");
    //    }
    //    Sequence seq = DOTween.Sequence();
    //    seq.Append(transform.DOMove(pos1.position, 1)).SetEase(Ease.Linear);
    //    seq.AppendCallback(() =>
    //    {
    //        CameraBase.S.DoEnd();
    //    });
    //seq.Append(transform.DOMove(pos2.position, 2)).SetEase(Ease.Linear);
    //seq.AppendCallback(() =>
    //{
    //playerCtl.fsm.SetNextState("Finish");
    //bool state = GameManager.S.data.PercentCharm > 0.74f;
    //playerCtl.anim.SetBool(state ? "Lv1Win" : "Lv1Fail", true);
    //foreach (var each in npcAnims)
    //{
    //    each.SetTrigger(state ? "Win" : "Fail");
    //    each.transform.DORotateQuaternion(Quaternion.LookRotation((state ? playerCtl.transform.position - each.transform.position : each.transform.position - playerCtl.transform.position), Vector3.up), 0.3f);
    //    each.transform.DOBlendableLocalMoveBy(each.transform.forward * 1.5f * (state ? 1 : -0.5f), 1).SetDelay(0.3f).OnComplete(() =>
    //    {
    //        each.SetTrigger("End");
    //    });
    //}

    //Timer.S.DoWait(4).OnComplete(() =>
    //{
    //    GameManager.S.data.AddLevelNum();
    //    GameManager.S.GoNextLevel();
    //});

    //});
    //}

}
