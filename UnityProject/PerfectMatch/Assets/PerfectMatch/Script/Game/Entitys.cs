﻿using System;
using System.Collections.Generic;

public enum EquipType
{
    Hair,
    Jacket,
    Pants,
    Shoes,
    Decorate
}

public class EquipEntity
{
    public int Id;
    public string NickName;
    public string MatName;
    public EquipEntity(int Id, string NickName, string MatName)
    {
        this.Id = Id;
        this.NickName = NickName;
        this.MatName = MatName;
    }
    public override string ToString()
    {
        string str = "Id: " + this.Id + " NickName: " + this.NickName + " MatName: " + this.MatName;
        return str;
    }
}

[Serializable]
public class HairEntity : EquipEntity
{
    public HairEntity(int Id, string NickName, string MatName) : base(Id, NickName, MatName)
    {

    }
}

[Serializable]
public class JacketEntity : EquipEntity
{

    public JacketEntity(int Id, string NickName, string MatName) : base(Id, NickName, MatName)
    {

    }
}

[Serializable]
public class PantsEntity : EquipEntity
{

    public PantsEntity(int Id, string NickName, string MatName) : base(Id, NickName, MatName)
    {

    }
}

[Serializable]
public class ShoesEntity : EquipEntity
{

    public ShoesEntity(int Id, string NickName, string MatName) : base(Id, NickName, MatName)
    {

    }
}

[Serializable]
public class DecorateEntity : EquipEntity
{

    public DecorateEntity(int Id, string NickName, string MatName) : base(Id, NickName, MatName)
    {

    }
}