﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidnightCoder.Game;
using Cinemachine;
using DG.Tweening;
public class CameraCtl2 : CameraBase
{
    public CinemachineBlendListCamera startBlendCamera;
    public GameObject phoneCamera;
    public GameObject gameCamera;
    public GameObject endCamera;
    public CinemachineBlendListCamera endBlendCamera;
    protected override void Awake()
    {
        base.Awake();
        float targetHight = 1334;
        if (750 * Screen.height > 1334 * Screen.width)
        {
            targetHight = 750 * Screen.height / Screen.width;
        }

        foreach (var c in startBlendCamera.GetComponentsInChildren<CinemachineVirtualCamera>())
        {
            c.m_Lens.FieldOfView *= (targetHight / 1334);
        }
        foreach (var c in phoneCamera.GetComponentsInChildren<CinemachineVirtualCamera>())
        {
            c.m_Lens.FieldOfView *= (targetHight / 1334);
        }
        foreach (var c in gameCamera.GetComponentsInChildren<CinemachineVirtualCamera>())
        {
            c.m_Lens.FieldOfView *= (targetHight / 1334);
        }
        foreach (var c in endCamera.GetComponentsInChildren<CinemachineVirtualCamera>())
        {
            c.m_Lens.FieldOfView *= (targetHight / 1334);
        }
        foreach (var c in endBlendCamera.GetComponentsInChildren<CinemachineVirtualCamera>())
        {
            c.m_Lens.FieldOfView *= (targetHight / 1334);
        }
    }
    protected override void Begin()
    {
        startBlendCamera.enabled = true;
        phoneCamera.SetActive(false);
        Timer.S.DoWait(3).OnComplete(() =>
        {
            gameCamera.SetActive(true);
            startBlendCamera.enabled = false;
            UIManager.S.OpenPanel(UIPanelType.MainPanel);
        });
    }

    public override void DoEnd()
    {
        endBlendCamera.enabled = true;
        gameCamera.SetActive(false);
 
    }

}
