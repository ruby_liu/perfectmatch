﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MidnightCoder.Game;

public class Level1End : LevelEndBase
{
    [SerializeField] private Transform pos1;
    [SerializeField] private Transform pos2;
    [SerializeField] private ParticleSystem kissFx;
    [SerializeField] private ParticleSystem noFx;
    [SerializeField] private ParticleSystem[] npcYesFx;
    [SerializeField] private ParticleSystem[] npcnoFx;

    private Animator[] npcAnims;
    protected override void Behaviour(PlayerCtl playerCtl)
    {
        npcAnims = pos2.GetComponentsInChildren<Animator>();
        foreach (var each in npcAnims)
        {
            each.SetTrigger("Ready");
        }
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOMove(pos1.position, 1)).SetEase(Ease.Linear);
        seq.AppendCallback(() =>
        {
            CameraBase.S.DoEnd();
        });
        seq.Append(transform.DOMove(pos2.position, 2)).SetEase(Ease.Linear);
        seq.AppendCallback(() =>
        {
            playerCtl.fsm.SetNextState("Finish");
            bool state = GameManager.S.data.PercentCharm > 0.74f;
            playerCtl.anim.SetBool(state ? "Lv1Win" : "Lv1Fail", true);
            if (state)
            {
                kissFx.Play(true);
            }
            else
            {
                noFx.Play(true);
            }
            foreach (var each in npcAnims)
            {
                if (state)
                {
                    foreach (var VARIABLE in npcYesFx)
                    {
                        VARIABLE.Play(true);
                    }
                }
                else
                {
                    foreach (var VARIABLE in npcnoFx)
                    {
                        VARIABLE.Play(true);
                    }
                }
                each.SetTrigger(state ? "Win" : "Fail");
                each.transform.DORotateQuaternion(Quaternion.LookRotation((state ? playerCtl.transform.position - each.transform.position : each.transform.position - playerCtl.transform.position), Vector3.up), 0.3f);
                each.transform.DOBlendableLocalMoveBy(each.transform.forward * 1.5f * (state ? 1 : -0.5f), 1).SetDelay(0.3f).OnComplete(() =>
                {
                    each.SetTrigger("End");
                });
            }

            Timer.S.DoWait(4).OnComplete(() =>
            {
                UIManager.S.ClosePanel(UIPanelType.MainPanel);
                GameManager.S.data.AddLevelNum();
                GameManager.S.GoNextLevel();
            });

        });
    }
}
