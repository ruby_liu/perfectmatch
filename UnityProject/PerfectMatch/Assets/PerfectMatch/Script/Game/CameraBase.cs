﻿using System;
using System.Collections;
using System.Collections.Generic;
using MidnightCoder.Game;
using UnityEngine;

public class CameraBase : TMonoSingleton<CameraBase>
{
    protected  virtual void Awake()
    {
        EventSystem.S.Register(EventID.OnTalkComplete, OnTalkComplete);
    }
    void OnDestroy()
    {
        EventSystem.S.UnRegister(EventID.OnTalkComplete, OnTalkComplete);
    }
    private void OnTalkComplete(int key, params object[] param)
    {
        Begin();
    }

    protected virtual void Begin()
    {

    }
    public virtual void DoEnd()
    {

    }
}
