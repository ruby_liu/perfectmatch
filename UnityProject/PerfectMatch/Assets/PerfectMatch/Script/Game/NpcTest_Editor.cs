﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcTest_Editor : MonoBehaviour
{
    [Title("半径")] public float radius = 1;
    //[Title("数量")] public int num = 1;
    public GameObject[] npc;
    [ContextMenu("生成NPC")]
    public void SpawnNpc()
    {
        int num = npc.Length;
        if (num == 0) return;

        float each = (360 - 60) / num;
        for (int i = 0; i < num; i++)
        {
            GameObject obj = GameObject.Instantiate<GameObject>(npc[i], transform);
            obj.transform.localPosition = Vector3.zero;
            obj.transform.localPosition = Pos(-35 + each * i);
            obj.transform.rotation = Quaternion.LookRotation(transform.position - obj.transform.position, Vector3.up);
        }
        

    }
    Vector3 Pos( float Angle)
    {
        float x1 = transform.localPosition.x + radius * Mathf.Cos(Angle * Mathf.PI / 180);
        float y1 = transform.localPosition.y + radius * Mathf.Sin(Angle * Mathf.PI / 180);
        return new Vector3(x1,0, y1);
    }
}
