﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidnightCoder.Game;
using DG.Tweening;
public class ItemBase : MonoBehaviour
{
    public EquipType Kind;
    public int Id = 1000;
    [HideInInspector] public bool isSpecialEquip = false; //是否是漂亮的装扮
    private Transform specialEquipGroup;
    void Awake()
    {
        EventSystem.S.Register(EventID.OnTalkComplete, OnTalkComplete);
        isSpecialEquip = Id % 1000 != 0;
    }
    private void OnDestroy()
    {
        EventSystem.S.UnRegister(EventID.OnTalkComplete, OnTalkComplete);
    }
    private void Update()
    {
        transform.localRotation = transform.localRotation * Quaternion.AngleAxis(Time.deltaTime*60, Vector3.up) ;
    }

    //根据选择路线不同，更新所有道路上装扮的显示
    private void OnTalkComplete(int key, params object[] param)
    {
        if (GameManager.S.data.CurLevel == 2 && isSpecialEquip)
        {
            int _selectId = GameManager.S.data.GetCurLvSelect();

            if (Kind == EquipType.Decorate)
            {
                if (_selectId == 0)
                {
                    gameObject.SetActive(false);
                    return;
                }
            }
            else
            {
                Id += (_selectId == 0 ? 0 : 1);
            }
        }
        //TODO:生成物品
        EquipEntity equipInfo = GameManager.S.data.GetEquipInfoById(Kind, Id);
        if (equipInfo == null)
        {
            transform.GetComponent<Collider>().enabled = false;
            return;
        }
        string mName = equipInfo.MatName;
        GameObject item = Instantiate(Resources.Load<GameObject>("Equips/" + mName),transform);
        item.transform.localPosition = Vector3.zero;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "PlayerTrigger") return;
        transform.GetComponent<Collider>().enabled = false;
        Destroy(gameObject);
        //transform.GetComponentInChildren<Renderer>().material.DOFade(0, 0.1f).OnComplete(() =>
        //    {
        //        Destroy(this.gameObject);
        //    });
    }
}
