﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidnightCoder.Game;
using DG.Tweening;
using TMPro;
public class PlayerCtl : TMonoSingleton<PlayerCtl>
{
    [HideInInspector] public PlayerTrigger trigger;    // 碰撞触发器
    private Transform body;
    private Transform weaponRoot;

    [HideInInspector] public PlayerMoveCtl playerMoveCtl;
    [HideInInspector] public Animator anim;

    [SerializeField] private GameObject phone;
    private ParticleSystem yesFx;
    private ParticleSystem noFx;

    [HideInInspector] public FSM fsm;

    void Awake()
    {
        InitComponent();
        EventSystem.S.Register(EventID.OnGameStart, OnGameStart);
        EventSystem.S.Register(EventID.OnTalkComplete, OnTalkComplete);
        EventSystem.S.Register(EventID.OnCharmChange, OnCharmChange);
    }
    void OnDestroy()
    {
        EventSystem.S.UnRegister(EventID.OnGameStart, OnGameStart);
        EventSystem.S.UnRegister(EventID.OnTalkComplete, OnTalkComplete);
        EventSystem.S.UnRegister(EventID.OnCharmChange, OnCharmChange);
    }
    void Start()
    {

    }

    void Update()
    {
        fsm.Update();
    }

    private void InitComponent()
    {
        body = transform.Find("Body");
        weaponRoot = phone.transform.parent;
        playerMoveCtl = GetComponent<PlayerMoveCtl>();
        trigger = transform.Find("Trigger").GetComponent<PlayerTrigger>();
        yesFx = transform.Find("YesFx").GetComponent<ParticleSystem>();
        noFx = transform.Find("NoFx").GetComponent<ParticleSystem>();
        anim = transform.GetComponent<Animator>();
        trigger.player = this;

        fsm = new FSM();
        fsm.AddState("Idle", IdleIn, IdleStay, IdleOut);
        fsm.AddState("Walk", WalkIn, WalkStay, WalkOut);
        fsm.AddState("Finish", FinishIn, FinishStay, FinishOut);
    }

    private void OnGameStart(int key, params object[] param)
    {
        playerMoveCtl.isGameStart = true;
        fsm.SetNextState("Walk");

    }
    private void OnTalkComplete(int key, params object[] param)
    {
        Timer.S.DoWait(1).OnComplete(() => { anim.SetTrigger("PutIn"); });
        Timer.S.DoWait(2).OnComplete(() => { phone.SetActive(false); });
        Timer.S.DoWait(3).OnComplete(() => { fsm.SetNextState("Idle"); });

        int cl = GameManager.S.data.CurLevel;
        if (cl == 2 && GameManager.S.data.GetCurLvSelect() == 1)
        {
            anim.SetInteger("IdleKind", 1);
        }
    }

    private TimerTween _curBlendTween;
    private void OnCharmChange(int key, params object[] param)
    {
        Timer.S.Stop(_curBlendTween);
        float _curBlendValue = anim.GetFloat("WalkBlend");
        float _tarBlendValue = GameManager.S.data.PercentCharm;
        float _sumTime = 0.5f;
        float _stepDeltaValue = (_tarBlendValue - _curBlendValue) / _sumTime;
        _curBlendTween = Timer.S.DoWait(_sumTime).OnUpdate(() =>
        {
            _curBlendValue += Time.deltaTime * _stepDeltaValue;
            anim.SetFloat("WalkBlend", _curBlendValue);
        }).OnComplete(() =>
        {
            anim.SetFloat("WalkBlend", _tarBlendValue);
        });
    }

    private void ChangeAppear(EquipType kind, int id)
    {
        EquipEntity equipInfo = GameManager.S.data.GetEquipInfoById(kind, id);
        Transform findRoot = kind == EquipType.Decorate ? weaponRoot : body;
        List<GameObject> allKindsObj = findRoot.FindsWithSubString(kind.ToString(), true);
        foreach (GameObject each in allKindsObj)
        {
            each.SetActive(each.name == equipInfo.MatName);
        }
    }
    private EquipEntity GetAppear(EquipType kind)
    {
        int curEquipId = GameManager.S.data.GetCurEquipId(kind);
        if (curEquipId < 0) return null;
        EquipEntity equipInfo = GameManager.S.data.GetEquipInfoById(kind, curEquipId);
        return equipInfo;
    }

    private void DoFinish()
    {
        playerMoveCtl.isGameStart = false;
        body.DOLocalRotate(Vector3.zero, 0.5f);
        transform.GetComponent<LevelEndBase>().Execute(this); ;
    }

    public void TriggerEnter(Collider other)
    {
        if (other.tag == "Item")
        {
            ItemBase item = other.GetComponent<ItemBase>();
            EquipType kind = item.Kind;
            int id = item.Id;
            bool isSpecial = item.isSpecialEquip;
            EquipIt(kind, id, isSpecial);
        }
        else if (other.tag == "Door")
        {
            foreach (var co in other.transform.parent.GetComponentsInChildren<BoxCollider>())
            {
                co.enabled = false;
                co.gameObject.SetActive(false);
            }

            foreach (var mat in other.transform.parent.Find("Model").GetComponent<Renderer>().materials)
            {
                mat.DOFade(0, 0.3f);
                float v = 1;
                DOTween.To(() => v, x => v = x, 0, 0.3f).OnUpdate(() => { mat.SetFloat("_Alpha", v); }).OnComplete(() =>
                    {
                        other.transform.parent.Find("Model").gameObject.SetActive(false);
                    });
            }

            other.transform.parent.Find("DoorGreen").gameObject.SetActive(false);
            other.transform.parent.Find("DoorRed").gameObject.SetActive(false);

            other.transform.parent.Find("Info").GetComponent<TextMeshPro>().DOFade(0, 0.3f);

            ChooseDoor door = other.GetComponent<ChooseDoor>();
            if (door.complete1Cloth)
            {
                yesFx.Play(true);
                //补位一件完美装
                for (int i = 0; i < 4; i++)
                {
                    EquipType t = (EquipType)i;
                    int _id = GameManager.S.data.GetCurEquipId(t);
                    if (_id % 1000 == 0)
                    {
                        if (GameManager.S.data.CurLevel == 2)
                        {
                            int _selectId = GameManager.S.data.GetCurLvSelect();
                            _id += (_selectId == 0 ? 2 : 3);
                        }
                        else
                        {
                            _id += 1;
                        }
                        EquipIt(t, _id, true);
                        break;
                    }
                }
            }
            else if (door.lose1Cloth)
            {
                noFx.Play(true);
                //丢失一件完美装
                for (int i = 0; i < 4; i++)
                {
                    EquipType t = (EquipType)i;
                    int _id = GameManager.S.data.GetCurEquipId(t);
                    if (_id % 1000 != 0)
                    {
                        _id = _id / 1000 * 1000;
                        EquipIt(t, _id, false);
                        break;
                    }
                }
            }
            else
            {
                EquipType kind = door.Kind;
                int id = door.Id;
                bool isSpecial = door.isSpecialEquip;
                EquipIt(kind, id, isSpecial);
            }
        }
        else if (other.tag == "Finish")
        {
            DoFinish();
        }
    }
    public void TriggerExit(Collider other)
    {
        if (other.tag == "")
        {

        }
    }

    private void EquipIt(EquipType kind, int id, bool isSpecial)
    {
        EquipEntity equipInfo = GetAppear(kind);
        if (equipInfo != null && equipInfo.Id == id)
        {
            //重复装饰

        }
        else
        {
            if (isSpecial)
            {
                GameManager.S.data.AddCharm();
            }
            else
            {
                GameManager.S.data.ReduceCharm();
            }
            ChangeAppear(kind, id);
            GameManager.S.data.SetCurEquipData(kind, id);
        }

        if (isSpecial)
        {
            yesFx.Play(true);
        }
        else
        {
            noFx.Play(true);
        }
    }

    #region FSM
    #region Idle
    private void IdleIn()
    {
        anim.SetBool("Idle", true);
    }
    private void IdleStay()
    {

    }
    private void IdleOut()
    {
        anim.SetBool("Idle", false);
    }
    #endregion
    #region Walk
    private void WalkIn()
    {
        anim.SetBool("Walk", true);
    }
    private void WalkStay()
    {

    }
    private void WalkOut()
    {
        anim.SetBool("Walk", false);
    }
    #endregion
    #region Finish
    private void FinishIn()
    {
        anim.SetBool("Idle", true);
    }
    private void FinishStay()
    {

    }
    private void FinishOut()
    {

    }
    #endregion
    //#region 
    //private void In()
    //{

    //}
    //private void Stay()
    //{

    //}
    //private void Out()
    //{

    //}
    //#endregion

    #endregion
}
