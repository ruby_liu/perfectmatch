﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ChooseDoor : MonoBehaviour
{
    public enum AnimType
    {
        UpDown,
        Rotate,
        Both
    }
    public EquipType Kind;
    public int Id = 1000;
    [HideInInspector] public bool isSpecialEquip = false; //是否是漂亮的装扮
    public bool complete1Cloth = false;
    public bool lose1Cloth = false;

    public AnimType animType;

    private void Awake()
    {
        isSpecialEquip = Id % 1000 != 0;
        switch (animType)
        {
            case AnimType.UpDown: UpDownAnim();break;
            case AnimType.Rotate: RotateAnim(); break;
            case AnimType.Both: UpDownAnim(); RotateAnim(); break;
        }
    }
    private void Update()
    {
    }

    private void UpDownAnim()
    {
        Transform trans = transform.GetChild(0);
        Sequence sequence = DOTween.Sequence();
        sequence.SetLoops(-1);
        sequence.Append(trans.DOLocalMoveY(-0.5f, 1).SetEase(Ease.InOutSine));
        sequence.Append(trans.DOLocalMoveY(0.5f, 1).SetEase(Ease.InOutSine));
    }
    private void RotateAnim()
    {
        transform.DOLocalRotate(Vector3.up*360, 4,RotateMode.FastBeyond360).SetEase(Ease.Linear).SetLoops(-1,LoopType.Restart);

    }
}
