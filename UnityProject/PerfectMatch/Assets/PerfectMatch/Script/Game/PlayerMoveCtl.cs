﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidnightCoder.Game;

public class PlayerMoveCtl : MonoBehaviour
{
    private CharacterController cc;
    private Transform body;
    [Title("前进速度")]
    public float forwardSpeed = 5;
    [Title("左右速度")]
    public float lrSpeed = 5;
    [Title("移速变化曲线")]
    public AnimationCurve moveSpeedCurve;
    private InputDir inputDir;
    [HideInInspector] public bool isGameStart = false;
    void Awake()
    {
        inputDir = new InputDir(1, 0);
        cc = GetComponent<CharacterController>();
        body = transform.Find("Body");
    }
    void Start()
    {

    }

    void Update()
    {
        if (!isGameStart) return;
        int d = 0;
        if (inputDir.right != 0)
        {
            d = inputDir.right > 0 ? 1 : -1;
        }
        Vector3 _desMoveDir = new Vector3(d  * lrSpeed * moveSpeedCurve.Evaluate(Mathf.Abs(inputDir.right)), 0, inputDir.forward * (forwardSpeed * 0.8f + forwardSpeed * 0.2f * GameManager.S.data.PercentCharm));
        cc.Move(transform.TransformDirection(_desMoveDir) * Time.deltaTime);

        float desAngle = 0;
        desAngle = 60 * Mathf.Clamp(inputDir.right,-1,1);
        body.localRotation = Quaternion.Lerp(body.localRotation, Quaternion.AngleAxis(desAngle, Vector3.up), Time.deltaTime * 5);
    }

    /// <summary>
    /// Joystick消息传进来的
    /// </summary>
    /// <param name="_dir"></param>
    public void SetDir(Vector2 _dir)
    {
        inputDir.right = _dir.x;
    }

    private class InputDir
    {
        public float forward;
        public float right;

        public InputDir(float forward, float right)
        {
            this.forward = forward;
            this.right = right;
        }
    }
}
