﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public enum RoleMoveDirection { None,Left,Right}

    public class RoleController : MonoBehaviour
    {
        #region 字段
        [Header("人物的移动速度")]
        [SerializeField] private float _moveSpeed;
        //缓存自身Transform
        private Transform _self;
        //人物移动方向枚举
        [HideInInspector]
        public RoleMoveDirection roleMoveDirection;
        //人物移动边界
        private float _moveBorder;
        [Header("当前是否可以进行玩家手势响应")]
        //是否可以手势响应--true可以，false不行
        public bool IsInputResponse;
        [Header("当前玩家是否正在操作")]
        //玩家是否正在操作--true正在操作,false没在操作
        public bool IsInputing;
        //玩家手势按下的位置
        private Vector3 _mousePos;
        //人物移动X轴
        private Vector3 _xDirection;
        //人物要挂载CharacterController组件
        private CharacterController characterController;
        [SerializeField]
        [Header("人物左右移动的速度")]
        //人物在X轴（左右）移动的速度
        private float _xSpeed;
        //人物的移动向量
        private Vector3 _moveVec3;
        [Header("第一关跑完之后要去的位置")]
        [SerializeField]private Transform _endTargetFirst;
        [Header("第一关到达跑完之后位置的时间")]
        [SerializeField]private float _toTargetTimeFirst;
        [Header("结局表现围观人群移动过去的时间")]
        [SerializeField]
        private float _peopleTime;
        [Header("第一个场景主角跑动Z轴超过多少算跑到，之后不受玩家操控")]
        [SerializeField]private float _firstEndValueZDir;
        [Header("第二个场景主角跑动Z轴超过多少算跑到，之后不受玩家操控")]
        [SerializeField] private float _secondEndValueZDir;

        //第二个场景终点的处理
        [Header("第二个场景主角到终点之后要去哪里")]
        [SerializeField]
        private Transform _endTargetSecond;
        [Header("第二个场景终点处主角多久到目的地")]
        [SerializeField]
        private float _toTargetTimeSecond;
        [Header("保镖")]
        [SerializeField]
        private Transform _bodyguard;
        [Header("记者")]
        [SerializeField]
        private Transform _reporter;
        #endregion

        #region 集合
        [Header("成功表现围观人群去的地方的列表")]
        [SerializeField]
        private List<Transform> _allAccessTargetList = new List<Transform>();
        [Header("失败表现围观人群去的地方的列表")]
        [SerializeField]
        private List<Transform> _allExitTargetList = new List<Transform>();
        [Header("围观人群的列表")]
        //围观人群的集合
        private List<Transform> _aLLPeopleList = new List<Transform>();
        #endregion

        #region 生命周期
        private void Start()
        {
            characterController = this.GetComponent<CharacterController>();
            IsInputResponse = true;
            _self = this.transform;
            StartCoroutine(nameof(RoleMoveActionCor));
        }

        private void Update()
        {
            RoleMove();
        }
        #endregion

        #region 协程
        private IEnumerator RoleMoveActionCor()
        {
            while (IsInputResponse)
            {
                //手势识别
                InputRecognition();
                //手势响应
                InputResponse();
                yield return null;
            }
        }
        #endregion

        #region 方法
        //手势识别
        private void InputRecognition()
        {
            roleMoveDirection = RoleMoveDirection.None;
            if (Input.GetMouseButtonUp(0))
            {
                //这里规避个玩家停止输入但是继续动的问题
                _xDirection.x = 0f;
                IsInputing = false;
            }
            //按下
            if (Input.GetMouseButtonDown(0))
            {
                IsInputing = true;
                _mousePos = Input.mousePosition;
            }
            //玩家处于按下状态时
            if (Input.GetMouseButton(0) && IsInputing)
            {
                Vector3 vec3 = Input.mousePosition - _mousePos;
                //玩家滑动一点距离才会响应，避免玩家误触
                if (vec3.magnitude > 0.3f)
                {
                    //根据玩家输入手势滑动在X轴上移动的角度来判定方向
                    var angleX = Mathf.Acos(Vector3.Dot(vec3.normalized, Vector2.right)) * Mathf.Rad2Deg;
                    if (angleX <= 45)
                    {
                        roleMoveDirection = RoleMoveDirection.Right;
                    }
                    else if (angleX >= 135)
                    {
                        roleMoveDirection = RoleMoveDirection.Left;
                    }
                }
            }
            //限定边界(两边)，防止人物跑出跑道
            if (_self.position.x >= _moveBorder)
            {
                _self.position = new Vector3(_moveBorder, _self.position.y, _self.position.z);
            }
            if (_self.position.x <= -_moveBorder)
            {
                _self.position = new Vector3(-_moveBorder, _self.position.y, _self.position.z);
            }
        }

        //手势响应
        private void InputResponse()
        {
            if (roleMoveDirection == RoleMoveDirection.Left)
            {
                _xDirection = Vector3.left;
            }
            else if (roleMoveDirection == RoleMoveDirection.Right)
            {
                _xDirection = Vector3.right;
            }
        }

        //人物移动
        private void RoleMove()
        {
            if (characterController != null && IsInputResponse)
            {
                _moveVec3.z = _moveSpeed;
                characterController.Move((_xDirection * _xSpeed + _moveVec3) * Time.deltaTime);
            }  
        }

        #region 结局表现
        //第一个场景终点相应处理
        private void SceneFirstEndingHandler()
        {
            //随便写一个120f后面结合场景换
            if (_self.position.z > _firstEndValueZDir)
            {
                //玩家不能操作了
                IsInputResponse = false;
                //播放主角走路的动画
                
                //人物过去
                _self.DOMove(_endTargetFirst.position, _toTargetTimeFirst).OnComplete(() =>
                {
                    //主角到达圈中心的时候人群的行为
                    PeopleAction(_aLLPeopleList, _peopleTime);
                });
            }
        }



        private void SceneSecondEndingHandler()
        {
            if (_self.position.z> _secondEndValueZDir)
            {
                IsInputResponse = false;
                _self.DOMove(_endTargetSecond.position, _toTargetTimeSecond).OnComplete(() =>
                {
                    //主角到达门口,---暴力路线还是侦探路线
                    
                });
            }
        }

        //暂时用1和2区分暴力路线和侦探路线
        private void ActionHandler(int value)
        {
            switch (value)
            {
                case 1:
                    //调用ForceAction();
                    break;
                case 2:
                    //调用DetectAction();
                    break;
            }
        }

        //暴力路线
        private void ForceAction(Vector3 bed,float time,float bodyguardTime)
        {
            //保镖播放破门动画

            _self.DOMove(bed, time).OnComplete(() =>
            {
                //主角播放球棒指向床的动画

                //保镖冲上去
                _bodyguard.DOMove(bed, bodyguardTime).OnComplete(() =>
                {
                    //保镖播放打人动画

                    //床上那俩人播放抱在一起哭的动画+飘表情

                    //EmotionMgr.S.CreateEmotion()
                });
            });
        }

        //侦探路线
        private void DetectAction(Vector3 bed,float reporterTime) 
        {
            //主角播放开锁动画

            _reporter.DOMove(bed, reporterTime).OnComplete(() =>
            {
                //记者播放拍照动画

                //飘表情
                //EmotionMgr.S.CreateEmotion()
            });

        }

        #endregion


        /// <summary>
        /// 结局表现:围观人群的行为
        /// </summary>
        /// <param name="list">人群的集合</param>
        /// <param name="time">到达目标点的时间</param>
        private void PeopleAction(List<Transform> list, float time)
        {
            Transform peopleTrans = null;
            for (int i = 0; i < list.Count; i++)
            {
                peopleTrans = list[i];
                if (peopleTrans == null) return;
                //播放围观人群走路的动画

                //如果结局是成功的，这里要加个根据属性值（魅力值）判断！！！！！！！！！！！！！！
                peopleTrans.DOMove(CheckTargetOccupy(_allAccessTargetList), time).OnComplete(() =>
                {
                    //人群到了之后....还做什么，换个动画？写在这里
                   
                });

                //如果失败会发生什么？
                peopleTrans.DOMove(CheckTargetOccupy(_allExitTargetList), time).OnComplete(() =>
                {
                    //人群离开之后，还有什么处理在这里加

                });
            }
        }


        /// <summary>
        /// 检测目标位置是不是被占用了
        /// </summary>
        /// <param name="list">围观人群要走向的所有位置集合</param>
        /// <returns></returns>
        private Vector3 CheckTargetOccupy(List<Transform> list)
        {
            Vector3 vec3 = Vector3.zero;
            for (int i = 0; i < list.Count; i++)
            {
                Transform trans = list[i];
                if (trans != null)
                {
                    if (trans.GetComponent<AccessExitProperties>().AccessExitValue == 0)
                    {
                        //改变状态
                        trans.GetComponent<AccessExitProperties>().AccessExitValue = 1;
                        return trans.position;
                    }
                }
            }
            return vec3;
        }
        #endregion

    }

}
