﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//该脚本挂在结局表现时要去的目的地上，成功和失败的目的地都统一挂这个脚本
public class AccessExitProperties : MonoBehaviour
{
    //0：没被分配，1：已经被分配了
    public int AccessExitValue;
}
