﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using EZWork;
using MidnightCoder.Game;
using DG.Tweening;

//表情管理类，单例
public class EmotionMgr : TMonoSingleton<EmotionMgr>
{
    private void Awake()
    {
        //EZPool.S.Regist("起个池的名字", GetPrefab("填上Resources下预制体的路径"));
    }

    //配合对象池注册
    public GameObject GetPrefab(string prefabName)
    {
        GameObject prefab = Resources.Load<GameObject>(prefabName);
        if (!ReferenceEquals(prefab, null))
        {
            return prefab;
        }
        return null;
    }

    /// <summary>
    /// 创建表情
    /// </summary>
    /// <param name="pos">创建的位置</param>
    public void CreateEmotion(Vector3 pos)
    {
        GameObject obj = EZPool.S.Create("池的名字");
        if (obj != null)
        {
            obj.SetActive(true);
            obj.transform.position = pos;
        }
    }

    /// <summary>
    /// 表情飞到某处
    /// </summary>
    /// <param name="emotion">对象池创建出来的表情</param>
    /// <param name="targetObj">目标物体，例如表情创建在人物头顶，人物就是所谓的目标物体</param>
    /// <param name="value">Y轴（向上飞多少距离）</param>
    /// <param name="time">飞到的时间是多久</param>
    /// <param name="callBack">回调</param>
    public void EmotionGo(GameObject emotion,GameObject targetObj,float value,float time,Action callBack = null)
    {
        if (emotion != null && targetObj != null)
        {
            emotion.transform.DOMove(targetObj.transform.position += new Vector3(0f, value, 0f), time).OnComplete(() =>
            {
                emotion.SetActive(false);
            });
        }
    }
}
