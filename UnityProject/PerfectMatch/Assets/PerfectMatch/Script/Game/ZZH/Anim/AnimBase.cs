﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Qarth;

namespace GameWish.Game
{
    public class AnimBase
    {
        public Animator animator;
        /// <summary>
        /// 播放动画
        /// </summary>
        /// <param name="animFileName">动画名称</param>
        public virtual void PlayAnim(string animFileName)
        {
            if (animator != null)
            {
                animator.Play(animFileName);
            }
        }
    }

    public class AnimManager
    {
        private Dictionary<AnimEnum, string> animFileDic = new Dictionary<AnimEnum, string>();
        /// <summary>
        /// 注册动画文件到字典
        /// </summary>
        /// <param name="animEnum">动画枚举</param>
        /// <param name="animName">动画名称</param>
        public void RegistAnim(AnimEnum animEnum, string animName)
        {
            if (!string.IsNullOrEmpty(animName))
            {
                if (!animFileDic.ContainsKey(animEnum))
                {
                    animFileDic.Add(animEnum, animName);
                }
                else
                {
                    Debug.LogError("animFileDic已经包含key--> animEnum");
                }
            }
            else
            {
                Debug.LogError("animName是空嗷");
            }
        }
        /// <summary>
        /// 得到指定的动画文件
        /// </summary>
        /// <param name="animEnum">动画枚举</param>
        /// <returns>动画名称</returns>
        public string GetAppointAnimFile(AnimEnum animEnum)
        {
            string animFileName = "";
            foreach (AnimEnum animEnumType in animFileDic.Keys)
            {
                if (animEnum.Equals(animEnumType))
                {
                    animFileName = animFileDic[animEnumType];
                }
            }
            return animFileName;
        }
    }
}


