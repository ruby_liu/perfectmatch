﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using ZZHWork;
//using static ZZHWork.ZZHMessage;
//using Qarth;

//挂载在人物身上（有Animator组件的节点）
namespace GameWish.Game
{
    /// <summary>
    /// 动画枚举定义，后续动画在这里添加即可
    /// </summary>
    public enum AnimEnum
    {
        None,
        //添加动画枚举
        RoleWalkAnim,
        Max
    }

    public class PlayerCtrl : MonoBehaviour
    {
        private Animator animator;
        PlayerState playerState;
        AnimManager animManager;

        #region 生命周期
        private void Awake()
        {
            //注册动画消息
            //EventSystem.S.Register(EventID.OnPlayWalkAnim,PlayWalkAnim);
        }

        private void Start()
        {
            //animator = this.transform.GetChild(1).GetComponent<Animator>();
            animator = this.GetComponent<Animator>();
            playerState = new PlayerState(animator);
            animManager = new AnimManager();

            #region 示例，任意一种新的动画，1.注册2.播放
            //记得在Unity里设置Walk动画名称为RoleWalkAnim
            animManager.RegistAnim(AnimEnum.RoleWalkAnim, "RoleWalkAnim");
            #endregion
        }
        #endregion
       
       #region 播放动画
        //播放人物Walk动画
        private void PlayWalkAnim(int key, object[] param)
        {
            if (animator != null)
            {
                playerState.PlayAnim(animManager.GetAppointAnimFile(AnimEnum.RoleWalkAnim));
            }
        }
       #endregion
    }

    public class PlayerState : AnimBase
    {
        public PlayerState(Animator animator_)
        {
            animator = animator_;
        }
    }

}

