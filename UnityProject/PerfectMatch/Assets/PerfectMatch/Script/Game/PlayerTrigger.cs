﻿using UnityEngine;

public class PlayerTrigger : MonoBehaviour
{
    [HideInInspector] public PlayerCtl player;
    void OnTriggerEnter(Collider other)
    {
        player?.TriggerEnter(other);
    }
    void OnTriggerExit(Collider other)
    {
        player?.TriggerExit(other);
    }

}
