﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollowCtl : MonoBehaviour
{
    private List<Transform> followPosList;
    [HideInInspector] public List<NpcCtl> npcs;
    private Transform body;
    private int curNum = 0;
    private float speed = 10;
    void Awake()
    {
        followPosList = new List<Transform>();
        body = transform.Find("Body");
    }
    void Start()
    {

    }

    void Update()
    {
        if (curNum > 0)
        {
            foreach (Transform each in followPosList)
            {
                each.localRotation = Quaternion.Lerp(each.localRotation, body.localRotation, Time.deltaTime * speed);
            }
        }
    }

    public List<Transform> GetFollowPosList()
    {
        return followPosList;
    }

    public Transform AddFollowPos(NpcCtl npc)
    {
        npcs.Add(npc);
        Transform obj = new GameObject().transform;
        obj.name = "Follow" + curNum;
        obj.parent = transform;
        obj.localPosition = Vector3.zero;
        obj.localEulerAngles = Vector3.zero;
        followPosList.Add(obj);
        curNum++;
        UpdateCalculatePos();
        return obj;
    }

    private void UpdateCalculatePos()
    {
        if (curNum == 1)
        {
            followPosList[0].localPosition = new Vector3(-1f, 0, -1f);
        }
        else if (curNum == 2)
        {
            followPosList[0].localPosition = new Vector3(-1f, 0, -1f);
            followPosList[1].localPosition = new Vector3(1f, 0, -1f);
        }
        else if (curNum == 3)
        {
            followPosList[0].localPosition = new Vector3(-1f, 0, -1f);
            followPosList[1].localPosition = new Vector3(1f, 0, -1f);
            followPosList[2].localPosition = new Vector3(0, 0, -3f);
        }
    }

}
