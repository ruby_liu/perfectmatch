﻿using System.Collections;
using System.Collections.Generic;
using MidnightCoder.Game;
using UnityEngine;
using DG.Tweening;
public class NpcCtl : MonoBehaviour
{
    [HideInInspector] public Transform body;
    [HideInInspector] public Animator anim;

    [HideInInspector] public bool startFollowPlayer = false;
    [HideInInspector] public Transform followTrans;
    public GameObject light;
    private bool triggeredFlag = false;
    private float speed = 3;

    void Awake()
    {
        EventSystem.S.Register(EventID.OnTalkComplete, OnTalkComplete);
    }
    void OnDestroy()
    {
        EventSystem.S.UnRegister(EventID.OnTalkComplete, OnTalkComplete);
    }

    void Update()
    {
        if (!startFollowPlayer) return;
        transform.position = Vector3.Lerp(transform.position, followTrans.position, Time.deltaTime * speed);
        transform.rotation = Quaternion.Lerp(transform.rotation, followTrans.rotation, Time.deltaTime * 10);
    }

    private void OnTalkComplete(int key, params object[] param)
    {
        int selectId = GameManager.S.data.GetCurLvSelect();
        for (int i = 0; i < transform.childCount; i++)
        {
            bool flag = i == selectId;
            Transform child = transform.GetChild(i);
            if (flag)
            {
                body = child;
                anim = body.GetComponent<Animator>();
            }
            child.gameObject.SetActive(flag);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (triggeredFlag) return;
        if (other.tag == "PlayerTrigger")
        {
            triggeredFlag = true;
            followTrans = PlayerCtl.S.transform.GetComponent<PlayerFollowCtl>()?.AddFollowPos(this);
            startFollowPlayer = true;
            anim.SetTrigger("Move");

            float temp = 1;
            float t = 1;
            float delta = (10 - temp) / t;
            Timer.S.DoWait(1).OnUpdate(() =>
            {
                temp += Time.deltaTime * delta;
                speed = temp;
            }).OnComplete(() =>
            {
                ReSetFollowSpeed();
            });
        }
    }

    public void SetFollowSpeed(float num)
    {
        speed = num;
    }
    public void ReSetFollowSpeed()
    {
        speed = 10;
    }

    public void DoLast()
    {
        int selectId = GameManager.S.data.GetCurLvSelect();
        if (selectId == 0)
        {
            //拍照
            anim.SetTrigger("End");
            Sequence sequence = DOTween.Sequence();
            sequence.SetLoops(-1, LoopType.Restart);
            sequence.AppendInterval(Random.Range(0.2f, 0.4f));
            sequence.AppendCallback(() => { light.SetActive(true); });
            sequence.AppendInterval(0.01f);
            sequence.AppendCallback(() => { light.SetActive(false); });
            sequence.AppendInterval(Random.Range(0.05f, 0.15f));
            sequence.AppendCallback(() => { light.SetActive(true); });
            sequence.AppendInterval(0.01f);
            sequence.AppendCallback(() => { light.SetActive(false); });
            sequence.AppendInterval(Random.Range(0.2f, 0.4f));
        }
        else
        {
            //打人
            Timer.S.DoWait(Random.Range(0.1f, 0.5f)).OnComplete(() => { anim.SetTrigger("End"); });
        }
    }
}
