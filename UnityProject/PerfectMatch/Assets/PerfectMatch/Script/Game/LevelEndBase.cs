﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndBase : MonoBehaviour
{

    public void Execute(PlayerCtl playerCtl)
    {
        Behaviour(playerCtl);
    }

    protected virtual void Behaviour(PlayerCtl playerCtl)
    {

    }
}
