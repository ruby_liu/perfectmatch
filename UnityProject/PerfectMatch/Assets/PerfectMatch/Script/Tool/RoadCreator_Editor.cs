﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MidnightCoder.Game;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class RoadCreator_Editor : MonoBehaviour
{
    [SerializeField, Title("道路")] private GameObject roadPrefab;
    [SerializeField, Title("新建道路数量")] private int roadNum = 1;
    [ContextMenu("新建道路")]
    public void CreateRoad()
    {
        Bounds bounds = Util.GetBounds(roadPrefab);
        float zLen = bounds.size.z;
        Vector3 originPos = Vector3.zero - Vector3.forward * zLen;
        for (int i = 0; i < roadNum; i++)
        {
            int childCount = transform.childCount;
            if (childCount > 0)
            {
                originPos = transform.GetChild(childCount - 1).position;
            }
            Spawn(roadPrefab, originPos + Vector3.forward * zLen, "Road");
        }
    }
    [SerializeField, Title("结尾")] private GameObject finishPrefab;
    [ContextMenu("新建结尾")]
    public void CreateFinish()
    {
        Vector3 originPos = Vector3.zero;
        int childCount = transform.childCount;
        if (childCount > 0)
        {
            originPos = transform.GetChild(childCount - 1).position;
        }
        Spawn(finishPrefab, originPos, "Finish");
    }

    private void Spawn(GameObject prefab, Vector3 pos, string _name)
    {
        GameObject obj;
#if UNITY_EDITOR
        obj = PrefabUtility.InstantiatePrefab(prefab, this.transform) as GameObject;
#else
        obj = Instantiate(prefab, this.transform) as GameObject;
#endif
        obj.transform.position = pos;
        obj.name = _name;
    }

}
