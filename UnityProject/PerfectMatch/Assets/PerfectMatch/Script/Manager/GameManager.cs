﻿using System.Collections;
using UnityEngine;
using MidnightCoder.Game;
using UnityEngine.SceneManagement;
public class GameManager : TMonoSingleton<GameManager>
{
    public DataManager data;
    public AsyncOperation ao;

    void Awake()
    {
        data = new DataManager();
    }
    void Start()
    {
        GoNextLevel();
    }
    void Update()
    {

    }

    private void StartGamePlay()
    {

    }

    public void GoNextLevel()
    {
        ChangeScene("Level" + data.CurLevel);
    }
    private void ChangeScene(string sceneName)
    {
        data.ResetInGameData();
        UIManager.S.OpenPanel(UIPanelType.LoadPanel, () =>
       {
           StartCoroutine("LoadSceneIE", sceneName);
       });
    }

    IEnumerator LoadSceneIE(string sceneName)
    {
        ao = SceneManager.LoadSceneAsync(sceneName);
        ao.allowSceneActivation = false;
        yield return ao;
        UIManager.S.ClosePanel();
        StartGamePlay();
    }
    void OnApplicationQuit()
    {
        data.SaveData();
    }
}
