﻿using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System;
using MidnightCoder.Game;
public class DataManager
{
    private int _curLevel = 1;
    public int CurLevel
    {
        get { return _curLevel; }
        set
        {
            int v = value;
            if (v <= 0)
            {
                v = maxLevel;
            }
            else if (v > maxLevel)
            {
                v = 1;
            }
            _curLevel = v;
        }
    }
    public int maxLevel = 2;

    private int _curMoney = 0;
    public int CurMoney => _curMoney;

    private bool _musicOn = true;
    public bool MusicOn
    {
        get { return _musicOn; }
        set { _musicOn = value; }
    }
    public bool _buzzOn = true;
    public bool BuzzOn
    {
        get { return _buzzOn; }
        set { _buzzOn = value; }
    }
    public Dictionary<EquipType, List<EquipEntity>> equipDict;
    //public List<HairEntity> hairList;
    //public List<JacketEntity> jacketList;
    //public List<PantsEntity> pantsList;
    //public List<ShoesEntity> shoesList;
    //public List<DecorateEntity> decorateList;

    //关卡内数据
    private int _curCharm = 0;
    public int CurCharm => _curCharm;
    private int _maxCharm = 4;
    public int MaxCharm => _maxCharm;

    public float PercentCharm
    {
        get
        {
            return (float)_curCharm / (float)_maxCharm;
        }
    }

    private Dictionary<EquipType, int> curEquipDict;

    private List<int> _lvSelectId = new List<int>() { 0, 0 };

    public DataManager()
    {
        InitData();
    }
    private void InitData()
    {
        equipDict = new Dictionary<EquipType, List<EquipEntity>>();
        curEquipDict = new Dictionary<EquipType, int>();

        if (PlayerPrefs.HasKey("CurLevel"))
        {
            _curLevel = PlayerPrefs.GetInt("CurLevel");
        }
        if (PlayerPrefs.HasKey("MusicOn"))
        {
            _musicOn = PlayerPrefs.GetInt("MusicOn") == 1;
        }
        if (PlayerPrefs.HasKey("BuzzOn"))
        {
            _buzzOn = PlayerPrefs.GetInt("BuzzOn") == 1;
        }

        LoadData();
        ResetInGameData();
    }

    private void LoadData()
    {
        equipDict.Add(EquipType.Hair, new List<EquipEntity>());
        equipDict.Add(EquipType.Jacket, new List<EquipEntity>());
        equipDict.Add(EquipType.Pants, new List<EquipEntity>());
        equipDict.Add(EquipType.Shoes, new List<EquipEntity>());
        equipDict.Add(EquipType.Decorate, new List<EquipEntity>());
        string jsonText = Resources.Load<TextAsset>("Config/Equip").text;
        JsonData jsonData = JsonMapper.ToObject(jsonText);

        JsonData hairJsonData = jsonData["Hair"];
        for (int i = 0; i < hairJsonData.Count; i++)
        {
            JsonData _data = hairJsonData[i];
            equipDict[EquipType.Hair]
                .Add(new HairEntity((int)_data["Id"], (string)_data["NickName"], (string)_data["MatName"]));
        }
        JsonData jacketJsonData = jsonData["Jacket"];
        for (int i = 0; i < jacketJsonData.Count; i++)
        {
            JsonData _data = jacketJsonData[i];
            equipDict[EquipType.Jacket]
                .Add(new JacketEntity((int)_data["Id"], (string)_data["NickName"], (string)_data["MatName"]));
        }
        JsonData pantsJsonData = jsonData["Pants"];
        for (int i = 0; i < pantsJsonData.Count; i++)
        {
            JsonData _data = pantsJsonData[i];
            equipDict[EquipType.Pants]
                .Add(new PantsEntity((int)_data["Id"], (string)_data["NickName"], (string)_data["MatName"]));
        }
        JsonData shoesJsonData = jsonData["Shoes"];
        for (int i = 0; i < shoesJsonData.Count; i++)
        {
            JsonData _data = shoesJsonData[i];
            equipDict[EquipType.Shoes]
                .Add(new ShoesEntity((int)_data["Id"], (string)_data["NickName"], (string)_data["MatName"]));
        }
        JsonData decorateJsonData = jsonData["Decorate"];
        for (int i = 0; i < decorateJsonData.Count; i++)
        {
            JsonData _data = decorateJsonData[i];
            equipDict[EquipType.Decorate]
                .Add(new DecorateEntity((int)_data["Id"], (string)_data["NickName"], (string)_data["MatName"]));
        }
    }


    public void AddCharm(int num = 1)
    {
        if (_curCharm == _maxCharm) return;
        _curCharm = Mathf.Clamp(_curCharm + num, 0, _maxCharm);
        //TODO:发事件
        EventSystem.S.Send(EventID.OnCharmChange);
    }
    public bool ReduceCharm(int num = 1)
    {
        bool flag = true;
        if (_curCharm == 0) flag = false;
        _curCharm = Mathf.Clamp(_curCharm - num, 0, _maxCharm);
        //TODO:发事件
        EventSystem.S.Send(EventID.OnCharmChange);
        return flag;
    }

    public void ResetInGameData()
    {
        _curCharm = 0;
        _lvSelectId = new List<int>() { 0, 0 };
        curEquipDict.AddAndCover(EquipType.Hair, 1000);
        curEquipDict.AddAndCover(EquipType.Jacket, 2000);
        curEquipDict.AddAndCover(EquipType.Pants, 3000);
        curEquipDict.AddAndCover(EquipType.Shoes, 4000);
        curEquipDict.AddAndCover(EquipType.Decorate, -1);
    }
    public EquipEntity GetEquipInfoById(EquipType _type, int Id)
    {
        List<EquipEntity> _equipList = equipDict.TryGet(_type);
        for (int i = 0; i < _equipList.Count; i++)
        {
            if (_equipList[i].Id == Id)
            {
                return _equipList[i];
            }
        }
        return null;
    }

    public int GetCurEquipId(EquipType kind)
    {
        return curEquipDict.GetValue(kind);
    }

    public void SetCurEquipData(EquipType kind, int id)
    {
        curEquipDict[kind] = id;
    }

    public void SetCurLvSelect(int index)
    {
        _lvSelectId[_curLevel - 1] = index;
    }
    public int GetCurLvSelect()
    {
        return _lvSelectId[_curLevel - 1];
    }

    public void AddLevelNum()
    {
        CurLevel++;
    }

    public void AddMoneyNum(int num)
    {
        this._curMoney += num;
    }
    public void SaveData()
    {
        Debug.Log("存储了");
        PlayerPrefs.SetInt("CurLevel", _curLevel);
        PlayerPrefs.SetInt("MusicOn", _musicOn ? 1 : 2);
        PlayerPrefs.SetInt("BuzzOn", _buzzOn ? 1 : 2);
    }
}
