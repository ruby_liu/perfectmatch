using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MidnightCoder.Game;
    public class ChatItemBase : MonoBehaviour
    {
        [SerializeField]
        private RectTransform m_ChatTextBg;
        [SerializeField]
        private Text m_ChatStrText;
        //[SerializeField]
        // private RawImage m_ChatImg;
        [SerializeField]
        private RawImage m_HeadIconImg;
        [SerializeField]
        private RectTransform m_Root;

        /// <summary>
        /// 文字位置在X轴的偏移
        /// </summary>
        [SerializeField] private float m_TextPosOffsetX = 0;

        /// <summary>
        /// ABTest图片
        /// </summary>
       // [SerializeField]
        //private Sprite[] m_Sprite_AB = new Sprite[2];
        //[SerializeField]
        //private Image m_Image;
        //[SerializeField]
        //private Color[] m_UserColor_AB;
        //[SerializeField]
        //private bool isUserChat;

        private float m_TextMaxWidth = 455;
        private float m_TextParentWidthOffset = 50;
        private float m_TextParentHeightOffset = 0;

        private void Start()
        {
            ShowStartEffect();
            //UpdateChatTextContent("There's a There's a There's a ");
            //UpdateChatTextContent("There's a There's a There's a There's a There's a There's a There's a There's a There's a There's a There's a There's a There's a  ");
        }

        ///// <summary>
        ///// 变量指向AB面板不同元素,并显示A或B
        ///// </summary>
        //public void ChooseNomalOrWeChat(bool isNomal)
        //{
        //    if (isNomal)
        //    {
        //        m_Image.color = m_UserColor_AB[0];
        //        if (isUserChat)
        //        {
        //            m_Image.sprite = m_Sprite_AB[0];
        //        }
        //    }
        //    else
        //    {
        //        m_Image.color = m_UserColor_AB[1];
        //        if (isUserChat)
        //        {
        //            m_Image.sprite = m_Sprite_AB[1];
        //        }
        //    }
        //}


        private void ShowStartEffect()
        {
            m_Root.DOLocalMoveY(0, 0.5f);
        }

        public void UpdateChatTextContent(string str)
        {
            m_ChatStrText.text = str;

            RectTransform textRT = m_ChatStrText.GetComponent<RectTransform>();

            RectTransform textParentRT = m_ChatTextBg.GetComponent<RectTransform>();

            if (m_ChatStrText.preferredWidth > m_TextMaxWidth)
            {
                textRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_TextMaxWidth);

                textParentRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_TextMaxWidth + m_TextParentWidthOffset);
            }
            else
            {
                textRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_ChatStrText.preferredWidth);

                textParentRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_ChatStrText.preferredWidth + m_TextParentWidthOffset);
            }

            textRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, m_ChatStrText.preferredHeight);
            textParentRT.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, m_ChatStrText.preferredHeight + m_TextParentHeightOffset);

            textRT.anchoredPosition = new Vector2(-textRT.rect.width * .5f + m_TextPosOffsetX, textRT.rect.height * .5f);

            float height = m_ChatStrText.rectTransform().sizeDelta.y;
            m_ChatTextBg.sizeDelta = new Vector2(m_ChatTextBg.sizeDelta.x, height + 30.0f);

            this.rectTransform().sizeDelta = new Vector2(this.rectTransform().sizeDelta.x, height +50);

            m_ChatTextBg.gameObject.SetActive(true);
            //   m_ChatImg.gameObject.SetActive(false);
        }

        //public void UpdateChatImg(string texName)
        //{
        //    Texture tex = ChatMgr.S.LoadTextureByName(texName);

        //    m_ChatImg.texture = tex;
        //    m_ChatImg.SetNativeSize();

        //    if (tex != null)
        //    {
        //        this.rectTransform().sizeDelta = new Vector2(this.rectTransform().sizeDelta.x, tex.height + 50.0f);
        //    }

        //    m_ChatTextBg.gameObject.SetActive(false);
        //    m_ChatImg.gameObject.SetActive(true);
        //}

        public void UpdateHeadIcon(Texture tex)
        {
            m_HeadIconImg.texture = tex;
        }
    }