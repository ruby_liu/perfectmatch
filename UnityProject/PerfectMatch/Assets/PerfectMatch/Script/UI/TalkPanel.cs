﻿using System.Diagnostics.Tracing;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using MidnightCoder.Game;

public class TalkPanel : MonoBehaviour
{
    [SerializeField] private Text titleText;
    [HideInInspector] public GameObject npcTalkItemPrefab;
    [HideInInspector] public GameObject playerTalkItemPrefab;
    [HideInInspector] public TalkCtl talkCtl;

    private ScrollRect scrollView;
    private Transform selectGroup;
    private Button[] selectBtns;

    private List<ConversationNode> curLvTalkList;
    private ConversationNode curTalkPiece;
    private ConversationNode nextTalkPiece;

    private int curLv;



    void Awake()
    {
        npcTalkItemPrefab = Resources.Load<GameObject>("NPCChatItem");
        playerTalkItemPrefab = Resources.Load<GameObject>("PlayerChatItem");
        talkCtl = this.GetComponent<TalkCtl>();
        scrollView = transform.GetComponentInChildren<ScrollRect>();
        selectGroup = transform.Find("MainBg/SelectBg");
        selectBtns = selectGroup.GetComponentsInChildren<Button>(true);
    }

    void Start()
    {
        curLv = GameManager.S.data.CurLevel;

        curLvTalkList = talkCtl.talk.Level[curLv - 1].Talk;
        titleText.text = talkCtl.talk.Level[curLv - 1].Name;

        curTalkPiece = curLvTalkList[0];

        Step1(0.5f);

    }

    void Update()
    {

    }

    private async void Step1(float t)
    {
        await CustomAwaiter.WaitForAction<float>(WaitTime, t);

        ChatItemBase cib = GameObject.Instantiate(curTalkPiece.Belong == ConversationBelong.Player ? playerTalkItemPrefab : npcTalkItemPrefab, scrollView.content).GetComponent<ChatItemBase>();
        cib.UpdateChatTextContent(curTalkPiece.Value);

        if (curTalkPiece.NextId != null)
        {
            if (curTalkPiece.NextBelong == ConversationBelong.Player)
            {
                UpdatePlayerSelectShow();
            }
            else
            {
                curTalkPiece = curTalkPiece.GetNextConversationList()[0];
                Step1(1);
            }
        }
        else
        {
            Timer.S.DoWait(1).OnComplete(() => { EventSystem.S.Send(EventID.OnTalkComplete); });
        }

    }

    private void WaitTime(CustomAwaiter awaiter, float t)
    {
        Timer.S.DoWait(t).OnComplete(() => { awaiter.Complete(); });
    }

    private void UpdatePlayerSelectShow()
    {
        List<ConversationNode> selectList = curTalkPiece.GetNextConversationList();
        //Debug.LogError(curTalkPiece.Value);
        for (int i = 0; i < selectList.Count; i++)
        {
            selectBtns[i].GetComponentInChildren<Text>(true).text = selectList[i].Value;
            selectBtns[i].gameObject.SetActive(true);
        }
        selectGroup.gameObject.SetActive(true);
    }


    public void OnSelectBtnClick(int index)
    {
        GameManager.S.data.SetCurLvSelect(index);
        selectGroup.gameObject.SetActive(false);
        curTalkPiece = curTalkPiece.GetNextConversationList()[index];
        Step1(0.1f);
    }


}

