﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MidnightCoder.Game;
using System;

public class ChatPanel : BasePanel
{
    #region UI组件
    //yes or no button，记得在unity里拖一下
    [SerializeField]
    private Button _yesBtn;
    [SerializeField]
    private Button _noBtn;
    [SerializeField]
    private Text _yesAnswer;
    [SerializeField]
    private Text _yesMomAnswer;
    [SerializeField]
    private Text _noAnswer;
    [SerializeField]
    private Text _noMomAnswer;
    #endregion

    //想要多长时间回复
    [SerializeField]
    private float _time;

    private void Awake()
    {
        _yesBtn.onClick.AddListener(() => {
            OnYesButtonClick();
            Debug.Log("Yes点击事件add上去了嗷");
        });

        _noBtn.onClick.AddListener(() => {
            OnNoButtonClick();
            Debug.Log("No点击事件add上去了嗷");
        });
    }

    #region button event
    private void OnYesButtonClick()
    {
        GameObjectShow(_yesAnswer.gameObject);
        Invoke(nameof(MomYesAnswer), _time);
    }

    private void OnNoButtonClick()
    {
        GameObjectShow(_noAnswer.gameObject);
        Invoke(nameof(MomNoAnswer), _time);
    }
    #endregion

    private void GameObjectShow(GameObject obj) {
        if (!ReferenceEquals(obj,null))
        {
            obj.SetActive(true);
        }
    }

    private void MomYesAnswer()
    {
        GameObjectShow(_yesMomAnswer.gameObject);
    }

    private void MomNoAnswer()
    {
        GameObjectShow(_noMomAnswer.gameObject);
    }

    //private void GameObjectHide(GameObject obj)
    //{
    //    if (!ReferenceEquals(obj,null))
    //    {
    //        obj.SetActive(false);
    //    }
    //}

    #region override
    protected override void OnClose()
    {
        base.OnClose();
    }

    protected override void OnShow()
    {
        base.OnShow();
    }
    #endregion

}


