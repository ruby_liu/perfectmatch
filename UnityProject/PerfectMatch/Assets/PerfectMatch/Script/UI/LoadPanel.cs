﻿using UnityEngine.UI;
using UnityEngine;
public class LoadPanel : BasePanel
{
    private Slider progressBar;
    //  private float deltaValue = 0;
    private bool loading = false;
    void Awake()
    {
        progressBar = transform.Find("Slider").GetComponent<Slider>();
    }
    protected override void OnShow()
    {
        loading = true;
    }
    protected override void OnClose()
    {
        loading = false;
        progressBar.value = 0;
    }
    void Update()
    {
        if (!loading) return;
        UpdateProgressShow();
    }
    public void UpdateProgressShow()
    {
        float value = GameManager.S.ao.progress;
        if (value >= 0.9f)
        {
            value = 1;
            GameManager.S.ao.allowSceneActivation = true;
        }
        progressBar.value = value;
        // if (1 - progressBar.value < 0.01f)
        // {
        //     progressBar.value = 1;
        //     GameManager.S.ao.allowSceneActivation = true;
        // }
        // else
        // {
        //     progressBar.value = Mathf.Lerp(progressBar.value, value, 0.1f);
        // }

    }
}

