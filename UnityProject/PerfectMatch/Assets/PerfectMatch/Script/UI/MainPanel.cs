﻿using System.Diagnostics.Tracing;
using System;
using UnityEngine.UI;
using UnityEngine;
using MidnightCoder.Game;
using DG.Tweening;
public class MainPanel : BasePanel
{
    [HideInInspector] public Joystick joystick;

     private GameObject readyPop;

    private Slider charmProgress;

    void Awake()
    {
        readyPop = transform.Find("ReadyPop").gameObject;
        charmProgress = transform.GetComponentInChildren<Slider>();
        EventSystem.S.Register(EventID.OnCharmChange, OnCharmChange);
    }
    private void OnDestroy()
    {
        EventSystem.S.UnRegister(EventID.OnCharmChange, OnCharmChange);
    }
    protected override void OnShow()
    {
        joystick.Player = GameObject.FindWithTag("Player")?.transform;
        readyPop.SetActive(true);
        charmProgress.value = 0;
    }

    protected override void OnClose()
    {

    }

    //外部EventTrigger静态引用
    public void OnReadyPopClick()
    {
        readyPop.SetActive(false);
        EventSystem.S.Send(EventID.OnGameStart);
    }

    private void OnCharmChange(int key, params object[] param)
    {
        //charmProgress.value = GameManager.S.data.PercentCharm;
        charmProgress.DOValue(GameManager.S.data.PercentCharm, 0.3f);
    }

}

